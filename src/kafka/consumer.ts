import { Consumer, Kafka } from "kafkajs";

class KafkaConsumer {
  kafka: Kafka;
  consumer: Consumer;

  constructor() {
    this.kafka = new Kafka({
      clientId: "my-app",
      brokers: ["localhost:9092"],
    });

    this.consumer = this.kafka.consumer({
      groupId: "mygroup",
    });
  }
  async start() {
    await this.consumer.connect();
    console.log("Kafka consumer connected");
    await this.consumer.subscribe({
      topics: ["sample"],
      fromBeginning: false,
    });
    await this.consumer.run({
      eachMessage: async ({ topic, message, partition }) => {
        console.log("Message recieved", JSON.parse(message.value?.toString()??""));
      },
    });
  }
}

const kafkaConsumer = new KafkaConsumer();
export default kafkaConsumer;
