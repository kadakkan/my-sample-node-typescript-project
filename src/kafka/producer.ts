import { Kafka, KafkaConfig, Partitioners, Producer } from "kafkajs";

class KafkaProducer {
  kafka: Kafka;
  producer: Producer;
  constructor() {
    this.kafka = new Kafka({
      clientId: "my-app",
      brokers: ["localhost:9092"],
    });

    this.producer = this.kafka.producer({
      createPartitioner: Partitioners.LegacyPartitioner
    });
    this.producer.connect();
  }

  produce(topic: string, key: string, value: string, partition: number = 0) {
    return this.producer.send({
      topic: topic,
      messages: [
        {
          value,
          key,
          partition,
        },
      ],
    });
  }
}

const kafkaProducer = new KafkaProducer();
export default kafkaProducer;
