import { Request, Response } from "express";
import bookService from "../services/books.service";

class Book {
  async addBook(req: Request, res: Response): Promise<Response> {
    try {
      const newBook = req.body;
      const data = await bookService.addBook(newBook);
      return res
        .status(200)
        .json({ message: "Book added successfully", data });
    } catch (error) {
      console.log(error);
      return res
        .status(400)
        .json({ message: "Error occured while adding Books" });
    }
  }
}

const bookController = new Book();
export default bookController;
