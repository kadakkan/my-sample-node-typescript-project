import mongoose from "mongoose";
import { Book, BookGenre } from "../interfaces/books";
import BookModel from "../models/books";

class BookService {
    async addBook(newBook: Book) {
        try {
            console.log(newBook);
            return await BookModel.create(newBook);
        } catch (error) {
            console.log(error);
            throw new Error("Error while adding book");
        }
    }
}

const bookService = new BookService();

export default bookService;