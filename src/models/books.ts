import { Book, BookGenre } from "../interfaces/books";
import mongoose, { Schema } from "mongoose";

const bookSchema = new Schema<Book>({
  title: { type: String, required: true },
  genre: { type: String, required: true },
  author: { type: String, required: true },
  createdAt: { type: Date, default: Date.now },
});

const BookModel = mongoose.model<Book>("Book", bookSchema);

export default BookModel;
