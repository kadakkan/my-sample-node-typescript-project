import { Request, Response, Router } from "express";
import streamController from "../controllers/stream.controller";

const router = Router();

router.get("/video", (req: Request, res: Response) =>
  streamController.streamVideo(req, res)
);

export default router;
