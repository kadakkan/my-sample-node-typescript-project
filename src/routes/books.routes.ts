import { Request, Response, Router } from "express";
import { validate } from "../middleware/validate";
import { bookZodSchema } from "../interfaces/books";
import bookController from "../controllers/books.controller";

const router = Router();

router.get("/getBooks", (req: Request, res: Response) => {
  res.send([]);
});

router.post(
  "/addBooks",
  [validate(bookZodSchema)],
  (req: Request, res: Response) => bookController.addBook(req, res));

router.put("/editBooks/:id", (req: Request, res: Response) => {
  res.send("Editing Books");
});

export default router;
