import { Request, Response, Router } from "express";
import kafkaProducer from "../kafka/producer";

const router = Router();

router.post("/send-to-kafka", async (req: Request, res: Response) => {
  try {
    await kafkaProducer.produce(
      "sample",
      "first",
      JSON.stringify({ name: "sahad", age: 12 }),
      0
    );
    return res.json({ message: "Ran succesffully" });
  } catch (error) {
    return res.json({ message: "Some error occured" });
  }
});

export default router;
