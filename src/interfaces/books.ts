import mongoose, { Document, Schema } from "mongoose";
import { z } from "zod";

export interface Book  {
  _id: Schema.Types.ObjectId;
  title: string;
  genre: BookGenre;
  author: string;
  createdAt: Date;
}

export enum BookGenre {
  FICTION = "Fiction",
  NONFICTION = "Non-Fiction",
  MYSTERY = "Mystery",
  THRILLER = "Thriller",
  ROMANCE = "Romance",
}

export const bookZodSchema = z.object({
  body: z.object({
    title: z.string(),
    author: z.string(),
    genre: z.nativeEnum(BookGenre),
  }),
});
