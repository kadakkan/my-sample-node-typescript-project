import express from "express";
import cors from "cors";
import helmet from "helmet";
import dotenv from "dotenv";
import mongoose from "mongoose";

import booksRoutes from "./routes/books.routes";
import streamRoutes from "./routes/stream.routes";
import kafkaRoutes from "./routes/kafka.routes";

import kafkaConsumer from "./kafka/consumer";

kafkaConsumer.start();

dotenv.config();

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());
app.use(helmet({
  crossOriginResourcePolicy: false,
}));

app.use('/books',booksRoutes);
app.use('/stream',streamRoutes);
app.use('/kafka',kafkaRoutes);

app.get("/", function (req, res) {
  res.sendFile(__dirname + "/index.html");
});

const mongoUrl = process.env.MONGO_URI || "mongodb://localhost:27017/bookstore";
mongoose
  .connect(mongoUrl)
  .then(() => {
    console.log("Connected to Database");
  })
  .catch((error) => {
    console.log("Error connecting to database", error);
  });

const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});

